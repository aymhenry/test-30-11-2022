# Test 30-11-2022

This test aims to evaluate your skills both in elementary python (numpy/scikit-learn) and parameter optimization.
We ask that you allocate yourself only 1h to carry out this test (excluding the setup). It is expected that you will not finish it entirely, the remaining questions will serve as a support for Wednesday's interview.

In this test you are asked to run a linear regression on a sample of data.

$$\vec{y} = a\;\vec{x}+b$$

![ Scatter plot of the data](./figures/initial_data.svg)

⚠⚠ The solution to the test should be returned as a notebook named `test_<first_name>_<last_name>.ipynb` ⚠⚠

## Setup

1. Clone the test repository

```bash
git clone https://gitlab.com/aymhenry/test-30-11-2022
```

2. Install jupyter notebook
   Assuming you have python already installed on your machine, run ([more infos](https://jupyter.org/install)):

```bash
pip install notebook
```

If python is not installed, we recommend you install it via [anaconda](https://www.anaconda.com/).

3. Install the necessary libraries.

You are responsible for installing the libraries you might need during the test

4. Load the data

To save up some storage memory on the git repository, we provide only the script to generate the data. Please run the following command before starting the first exercise.

```bash
python generate_data.py
```

## Exercise 1: Linear regression library

Find a library that allows you to fit a linear model on the data. Run the optimization and plot on the same figure a line curve of the model along with a scatter plot of the original data `data/data.csv`.

The figure name is `exercise_1.pdf` and should be labeled properly.

## Exercise 2: Cost function

In the above exercise you used a library that handled all the regression steps for you. One often uses more complex models and cannot rely on libraries that take care of everything.

To compare two models together and choose the best, a computer needs a score (we want high scores) or a cost function (we want low cost). Complete the following function so that the cost decreases when the predictions of the model $\vec{y}$ get closer to the original data $\vec{y}_{data}$.

```python
def cost_function(y1,y2):
   # ...
   raise NotImplementedError("Define the cost fonction")
   # return cost
```

## Exercise 3: Gradient descent

There exists multiple techniques to optimize a model. A very common one is [gradient descent](https://en.wikipedia.org/wiki/Gradient_descent) where the parameters are iteratively updated according to the following formula:

$$\vec{p}_{new} = \vec{p}_{old} - \gamma \vec{\nabla}\text{Cost}_{\vec{p}_{old}}$$
where

- $\vec{p}_{old}$: old set of parameters
- $\vec{p}_{new}$: new set of parameters
- $\gamma$: learning rate
- $\vec{\nabla}\text{Cost}_{\vec{p}_{old}}$: the gradient of the cost function with the old parameters

### a) Define the gradient function

Define a gradient function that gives the derivatives of the cost function with respect to the parameters. You can compute the gradient numerically. In this specific problem we know the actual analytic expression of the model. In such a case, the gradient can also be determined analytically. Both choices are good.

```python
def gradient(a, b, x_data, y_data):
   # ...
   raise NotImplementedError("Define the gradient")
   # return np.array([grad_a, grad_b])
```

### b) Write a gradient descent algorithm that uses the above function. 

You are free to play around with the number of iteration and learning parameters so that the resulting model is satisfying.

```python
def gradient_descent(x_data, y_data, gamma, N_iter):
   a, b = [1, 1]
   # ...
   raise NotImplementedError("Define the gradient descent algorithm")
   return a, b
```

**Note:** It is not necessarily expected to find the same level of accuracy as the standard libraries.

### c) Generate a figure similar to exercise 1, but use the parameters found by your algorithm.
