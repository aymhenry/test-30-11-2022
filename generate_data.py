import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

N = 100
a = 0.5
b = -2


np.random.seed(12)

x_data = np.linspace(0, 10, N)
y_data = a * x_data + b + np.random.normal(0, 1, size=N)
df = pd.DataFrame({"X": x_data, "Y": y_data})

plt.figure()
plt.scatter(df["X"], df["Y"])
plt.xlabel("X")
plt.ylabel("Y")
plt.title("Data")
plt.savefig("figures/initial_data.svg")

df.to_csv("data/data.csv", index=False)
